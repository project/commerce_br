<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesCondition;

use Drupal\business_rules\ConditionInterface;
use Drupal\business_rules\Events\BusinessRulesEvent;
use Drupal\business_rules\ItemInterface;
use Drupal\business_rules\Plugin\BusinessRulesConditionPlugin;
use Drupal\commerce\ConditionGroup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The business rules for commerce order item condition.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesCondition
 *
 * @BusinessRulesCondition(
 *   id = "business_rules_commerce_order_item_condition",
 *   label = @Translation("Commerce order item condition"),
 *   group = @Translation("Commerce"),
 *   description = @Translation("Commerce order item condition."),
 *   isContextDependent = TRUE,
 *   reactsOnIds = {},
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   hasTargetField = FALSE,
 * )
 */
class BusinessRulesCommerceOrderItemCondition extends BusinessRulesConditionPlugin {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array &$form, FormStateInterface $form_state, ItemInterface $item) {

    $settings['commerce_conditions'] = [
      '#type' => 'commerce_conditions',
      '#title' => $this->t('Applies to'),
      '#parent_entity_type' => 'business_rules_condition',
      '#entity_types' => ['commerce_order_item'],
      '#default_value' => is_array($item->getSettings('commerce_conditions')) ? $item->getSettings('commerce_conditions') : [],
    ];

    $settings['operator'] = [
      '#title' => $this->t('Condition operator'),
      '#type' => 'radios',
      '#options' => [
        'AND' => $this->t('All conditions must pass'),
        'OR' => $this->t('Only one condition must pass'),
      ],
      '#default_value' => $item->getSettings('operator') ?? 'OR',
      '#weight' => 100,
    ];

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions(ConditionInterface $condition) {

    $plugin_manager = \Drupal::service('plugin.manager.commerce_condition');
    $conditions = [];
    $commerce_conditions = $condition->getSettings('commerce_conditions');
    if (!is_null($commerce_conditions)) {
      foreach ($commerce_conditions as $commerce_condition) {
        $commerce_condition = $plugin_manager->createInstance($commerce_condition['plugin'], $commerce_condition['configuration']);
        $conditions[] = $commerce_condition;
      }
    }
    return $conditions;

  }

  /**
   * {@inheritdoc}
   */
  public function process(ConditionInterface $condition, BusinessRulesEvent $event) {
    $result = FALSE;

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $event->getArgument('entity');
    $conditions = $this->getConditions($condition);
    $order_conditions = array_filter($conditions, function ($commerce_condition) {
      /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $commerce_condition */
      return $commerce_condition->getEntityTypeId() === 'commerce_order_item';
    });
    $order_conditions = new ConditionGroup($order_conditions, $condition->getSettings('operator'));
    $result = $order_conditions->evaluate($order_item);
    return $result;
  }

}
