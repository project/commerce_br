<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce order place pre transition.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_order_place_pre",
 *   label = @Translation("On order place pre transition"),
 *   description = @Translation("Reacts on order place pre transition event."),
 *   group = @Translation("Commerce Order"),
 *   eventName = "business_rules.commerce_order_place_pre",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceOrderPlacePre extends BusinessRulesReactsOnPlugin {

}
