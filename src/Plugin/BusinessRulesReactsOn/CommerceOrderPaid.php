<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce order paid transition.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_order_paid",
 *   label = @Translation("On order paid transition"),
 *   description = @Translation("Reacts on order paid transition event."),
 *   group = @Translation("Commerce Order"),
 *   eventName = "business_rules.commerce_order_paid",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceOrderPaid extends BusinessRulesReactsOnPlugin {

}
