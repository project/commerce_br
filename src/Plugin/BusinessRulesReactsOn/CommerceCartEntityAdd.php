<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce cart entity add event.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_cart_entity_add",
 *   label = @Translation("On cart entity add event"),
 *   description = @Translation("Reacts on cart entity add event."),
 *   group = @Translation("Commerce Order Item"),
 *   eventName = "business_rules.commerce_cart_entity_add",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceCartEntityAdd extends BusinessRulesReactsOnPlugin {

}
