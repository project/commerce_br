<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce order cancel pre transition.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_order_cancel_pre",
 *   label = @Translation("On order cancel pre transition"),
 *   description = @Translation("Reacts on order cancel pre transition event."),
 *   group = @Translation("Commerce Order"),
 *   eventName = "business_rules.commerce_order_cancel_pre",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceOrderCancelPre extends BusinessRulesReactsOnPlugin {

}
