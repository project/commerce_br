<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce cart order item remove event.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_cart_order_item_remove",
 *   label = @Translation("On cart order item remove event"),
 *   description = @Translation("Reacts on cart order item remove event."),
 *   group = @Translation("Commerce Order Item"),
 *   eventName = "business_rules.commerce_cart_order_item_remove",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceCartOrderItemRemove extends BusinessRulesReactsOnPlugin {

}
