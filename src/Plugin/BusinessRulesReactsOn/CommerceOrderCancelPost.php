<?php

namespace Drupal\commerce_br\Plugin\BusinessRulesReactsOn;

use Drupal\business_rules\Plugin\BusinessRulesReactsOnPlugin;

/**
 * The business rules for commerce order cancel post transition.
 *
 * @package Drupal\business_rules\Plugin\BusinessRulesReactsOn
 *
 * @BusinessRulesReactsOn(
 *   id = "commerce_order_cancel_post",
 *   label = @Translation("On order cancel post transition"),
 *   description = @Translation("Reacts on order cancel post transition event."),
 *   group = @Translation("Commerce Order"),
 *   eventName = "business_rules.commerce_order_cancel_post",
 *   hasTargetEntity = TRUE,
 *   hasTargetBundle = FALSE,
 *   priority = 1000,
 * )
 */
class CommerceOrderCancelPost extends BusinessRulesReactsOnPlugin {

}
