<?php

namespace Drupal\commerce_br\EventSubscriber;

use Drupal\business_rules\Events\BusinessRulesEvent;
use Drupal\business_rules\Plugin\BusinessRulesReactsOnManager;
use Drupal\business_rules\Util\BusinessRulesProcessor;
use Drupal\commerce_cart\Event\CartOrderItemRemoveEvent;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A subscriber to trigger business rules on commerce events.
 */
class CommerceBusinessRulesEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The business rules processor.
   *
   * @var \Drupal\business_rules\Util\BusinessRulesProcessor
   */
  protected $businessRulesProcessor;

  /**
   * The reactsOnManager.
   *
   * @var \Drupal\business_rules\Plugin\BusinessRulesReactsOnManager
   */
  protected $reactsOnManager;

  /**
   * Constructs a new Event Subscriber object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\business_rules\Util\BusinessRulesProcessor $business_rules_processor
   *   The business rules processor.
   * @param \Drupal\business_rules\Plugin\BusinessRulesReactsOnManager $reacts_on_manager
   *   The business rules processor.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, BusinessRulesProcessor $business_rules_processor, BusinessRulesReactsOnManager $reacts_on_manager) {
    $this->eventDispatcher = $event_dispatcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->businessRulesProcessor = $business_rules_processor;
    $this->reactsOnManager = $reacts_on_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ENTITY_ADD => ['onCartEntityAdd', 100],
      CartEvents::CART_ORDER_ITEM_REMOVE => ['onCartOrderItemRemove', 100],
      OrderEvents::ORDER_PAID => ['onOrderPaid', 100],
      'commerce_order.place.pre_transition' => ['onOrderPlacePre', 100],
      'commerce_order.place.post_transition' => ['onOrderPlacePost', 100],
      'commerce_order.cancel.pre_transition' => ['onOrderCancelPre', 100],
      'commerce_order.cancel.post_transition' => ['onOrderCancelPost', 100],

    ];
  }

  /**
   * Trigger business rules on add item to cart transition event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The cart event.
   */
  public function onCartEntityAdd(CartEntityAddEvent $event) {
    if ($this->ruleExists('commerce_cart_entity_add')) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $entity */
      $entity = $event->getOrderItem();
      $this->triggerBusinessRulesEvent($entity, 'commerce_cart_entity_add');
    }
  }

  /**
   * Trigger business rules on remove item from cart transition event.
   *
   * @param \Drupal\commerce_cart\Event\CartOrderItemRemoveEvent $event
   *   The cart event.
   */
  public function onCartOrderItemRemove(CartOrderItemRemoveEvent $event) {
    if ($this->ruleExists('commerce_cart_order_item_remove')) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $entity */
      $entity = $event->getOrderItem();
      $this->triggerBusinessRulesEvent($entity, 'commerce_cart_order_item_remove');
    }
  }

  /**
   * Trigger business rules on order paid transition event.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The event.
   */
  public function onOrderPaid(OrderEvent $event) {
    if ($this->ruleExists('commerce_order_paid')) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $event->getOrder();
      $this->triggerBusinessRulesEvent($entity, 'commerce_order_paid');
    }
  }

  /**
   * Trigger business rules on order place pre transition event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onOrderPlacePre(WorkflowTransitionEvent $event) {
    if ($this->ruleExists('commerce_order_place_pre')) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $event->getEntity();
      $this->triggerBusinessRulesEvent($entity, 'commerce_order_place_pre');
    }
  }

  /**
   * Trigger business rules on order place post transition event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onOrderPlacePost(WorkflowTransitionEvent $event) {
    if ($this->ruleExists('commerce_order_place_post')) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $event->getEntity();
      $this->triggerBusinessRulesEvent($entity, 'commerce_order_place_post');
    }
  }

  /**
   * Trigger business rules on order cancel pre transition event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onOrderCancelPre(WorkflowTransitionEvent $event) {
    if ($this->ruleExists('commerce_order_cancel_pre')) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $event->getEntity();
      $this->triggerBusinessRulesEvent($entity, 'commerce_order_cancel_pre');
    }
  }

  /**
   * Trigger business rules on order cancel post transition event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onOrderCancelPost(WorkflowTransitionEvent $event) {
    if ($this->ruleExists('commerce_order_cancel_post')) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
      $entity = $event->getEntity();
      $this->triggerBusinessRulesEvent($entity, 'commerce_order_cancel_post');
    }
  }

  /**
   * Dispatch Business Rules event.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check conditions against.
   * @param string $plugin_id
   *   The ID the represents the plugin type.
   */
  public function triggerBusinessRulesEvent(ContentEntityInterface $entity, $plugin_id) {
    $reacts_on_definition = $this->reactsOnManager->getDefinition($plugin_id);
    $entity_type_id = $entity->getEntityTypeId();
    $event = new BusinessRulesEvent($entity, [
      'entity_type_id' => $entity_type_id,
      'bundle' => $entity->bundle(),
      'entity' => $entity,
      'entity_unchanged' => $entity->original,
      'reacts_on' => $reacts_on_definition,
      'loop_control' => $entity->getEntityTypeId() . $entity->id(),
    ]);
    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher */
    $this->eventDispatcher->dispatch($reacts_on_definition['eventName'], $event);
  }

  /**
   * Check if a Business Rule exists.
   *
   * @param string $reacts_on
   *   The Event Name.
   *
   * @return bool
   *   TRUE if the business rule exists, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function ruleExists($reacts_on) {
    $business_rule_entity = $this->entityTypeManager
      ->getStorage('business_rule')
      ->loadByProperties(
        ['reacts_on' => $reacts_on]
      );
    return !empty($business_rule_entity);
  }

}
