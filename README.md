CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Commerce Business Rules module is an extension for Business Rules module and allow site
administrators to define conditionally actions execution based on commerce events.

FEATURES
--------

 * New rules for commerce order place, cancel and paid events.
 * New conditions to include commerce conditions for order and order items.

REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Business Rules - https://www.drupal.org/project/business_rules
 * Commerce Order - https://www.drupal.org/project/commerce
 * (Optional) Commerce Conditions Plus -
 https://www.drupal.org/project/commerce_conditions_plus

INSTALLATION
------------

 * Install the Commerce Business Rules module as you would normally install a contributed
 Drupal module. Visit https://www.drupal.org/node/1897420 for further
 information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Clear caches.
    3. Navigate to Administration > Configuration > Workflow > Business rules to
       manage business rules.
    4. To create a new action, access the "Actions" tab at the Business Rules
       page and select "Add Action" button. For extensive documentation visit:
       https://www.drupal.org/node/2869626.
    5. To create a new condition, access the "Conditions" tab at the Business
       Rules page and select "Add Condition" button. For extensive documentation
       visit: https://www.drupal.org/node/2869845.
    6. To create a new variable, access the "Variables" tab at the Business
       Rules page and select "Add Variable" button. For extensive documentation
       visit: https://www.drupal.org/node/2869849.
    7. After you've created all actions, conditions, and variables for the
       business rule, it's time to put all together by creating a new rule.
       Access the "Rules" tab at the Business Rules page and select "Add Rule"
       button. For extensive documentation visit:
       https://www.drupal.org/node/2869861.

MAINTAINERS
-----------
* Lexsoft - https://www.drupal.org/u/lexsoft

